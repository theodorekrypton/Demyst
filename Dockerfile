FROM node:18-alpine3.15 as build

WORKDIR /frontend
COPY ./frontend /frontend

RUN cd /frontend && npm install && npm run build

FROM python:3.10-slim

WORKDIR /app
COPY ./backend /app
COPY --from=build /frontend/build /app/application/templates

RUN python setup.py install

ENTRYPOINT ["backend.py"]

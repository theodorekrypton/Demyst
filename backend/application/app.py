import os

from flask import Flask, request, jsonify, send_from_directory
from application.services import accounting_softwares, database, decision_engine

dir_path = os.path.dirname(os.path.realpath(__file__))

app = Flask(__name__, static_folder=os.path.join(dir_path, "templates"))


@app.after_request
def after_request(response):
    response.headers['Access-Control-Allow-Origin'] = "*"
    response.headers['Access-Control-Allow-Headers'] = "Content-Type"
    return response


@app.route('/')
@app.route('/<path:path>')
def catch_all(path=''):
    if path:
        paths = path.split('/')
        return send_from_directory(os.path.join(app.static_folder, *paths[:-1]), paths[-1])
    else:
        return send_from_directory(app.static_folder, "index.html")


def error_handler(fn):
    def _wrapped(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except Exception as ex:
            return jsonify({"error": str(ex)}), 500
    return _wrapped


@error_handler
@app.route('/application', methods=['POST'])
def create_application():
    data = request.get_json()
    business_name = data["businessName"]
    accounting_software = data["accountingSoftware"]
    token = data["token"]
    year_established = data["yearEstablished"]
    loan_amount = data["loanAmount"]
    try:
        bs = getattr(accounting_softwares, accounting_software)(token)\
            .get_balance_sheet(business_name, int(year_established))
        application_id = database.loan_application.create(business_name, year_established, bs, int(loan_amount))
    except AttributeError:
        return jsonify({'error': f"Accounting software {accounting_software} not supported"}), 500
    return jsonify({'application_id': application_id, 'balance_sheet': bs})


@error_handler
@app.route('/application/submit/<application_id>', methods=['POST'])
def submit_application(application_id):
    la = database.loan_application.get(application_id)
    if decision_engine.get_decision(la.business_name, la.year_established, la.profit_summary, la.pre_assessment):
        return jsonify({'result': True})
    else:
        return jsonify({'result': False})


if __name__ == '__main__':
    app.run(debug=True)

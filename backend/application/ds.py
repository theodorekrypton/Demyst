from dataclasses import dataclass


@dataclass
class BalanceSheetItem:
    year: int
    month: int
    profit_or_loss: int
    asset_value: int


@dataclass
class LoanApplication:
    business_name: str
    balance_sheet: list[BalanceSheetItem]
    loan_amount: int
    year_established: int
    pre_assessment: int = 20
    profit_summary: list[int] = None

    def _calculate_profit_summary(self):
        profit_summary = []

        temp_profit = 0
        year = self.year_established
        for item in self.balance_sheet:
            if item.year != year:
                profit_summary.append(temp_profit)
                temp_profit = 0
                year = item.year
            temp_profit += item.profit_or_loss

        return profit_summary

    def _calculate_pre_assessment(self):
        if len(self.balance_sheet) >= 12:
            last12 = self.balance_sheet[-12:]
            if sum((x.asset_value for x in last12)) / 12 > self.loan_amount:
                return 100
            elif sum((x.profit_or_loss for x in last12)) > 0:
                return 60
        return 20

    def __post_init__(self):
        self.profit_summary = self._calculate_profit_summary()
        self.pre_assessment = self._calculate_pre_assessment()

from application.services.accounting_softwares import base


class MYOB(base.AccountingSoftwareAPI):
    def __init__(self, token):
        super().__init__(token)

    def get_balance_sheet(self, business_name, year_established):
        return base.fake_balance_sheet(year_established)

from abc import ABC, abstractmethod
import datetime
import random
from application.ds import BalanceSheetItem


class AccountingSoftwareAPI(ABC):
    def __init__(self, token):
        self.token = token

    @abstractmethod
    def get_balance_sheet(self, business_name, year_established) -> [BalanceSheetItem]:
        pass


def _generate_balance_sheet_fn(profit_or_loss, asset_value):
    def inner(year_established):
        result = []
        current_time = datetime.datetime.now()

        for yr in range(year_established, current_time.year):
            for mo in range(12):
                result.append(BalanceSheetItem(
                    year=yr,
                    month=mo+1,
                    profit_or_loss=profit_or_loss,
                    asset_value=asset_value
                ))

        for mo in range(current_time.month):
            result.append(BalanceSheetItem(
                year=current_time.year,
                month=mo+1,
                profit_or_loss=profit_or_loss,
                asset_value=asset_value
            ))
        return result
    return inner


_balance_sheet_1 = _generate_balance_sheet_fn(100, 100)
_balance_sheet_2 = _generate_balance_sheet_fn(100, -100)
_balance_sheet_3 = _generate_balance_sheet_fn(-100, -100)


def fake_balance_sheet(year_established):
    """
    This method is only used to mock a balance sheet that fits the response schema stated in the assignment README.md
    :param year_established: a valid year number
    :return:
    """
    current_year = datetime.datetime.now().year
    if year_established > current_year:
        raise Exception(f"The beginning year {year_established} is invalid")
    generators = [_balance_sheet_1, _balance_sheet_2, _balance_sheet_3]

    return generators[random.randint(0, 2)](year_established)

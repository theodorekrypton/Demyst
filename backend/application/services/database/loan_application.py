from itertools import count
from threading import Lock

from application.ds import LoanApplication

_id_generator = count()
_lock = Lock()

_store = {}  # type: dict[str, LoanApplication]


def create(business_name, year_established, balance_sheet, loan_amount):
    application_id = str(next(_id_generator))
    with _lock:
        # should use a KV database if possible, but here we just use a dict to simulate
        _store[application_id] = LoanApplication(
            business_name=business_name,
            year_established=year_established,
            balance_sheet=balance_sheet,
            loan_amount=loan_amount
        )
    return application_id


def get(application_id):
    return _store.get(application_id)


def update(application_id, **kwargs):
    application = _store.get(application_id)
    if application is None:
        return
    with _lock:
        for k, v in kwargs.items():
            setattr(application, k, v)

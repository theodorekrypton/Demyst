import argparse

from application.app import app


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', type=str, default='0.0.0.0')
    parser.add_argument('--port', type=int, default=12345)
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()
    app.run(args.host, args.port)

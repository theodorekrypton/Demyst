import setuptools

setuptools.setup(
    name="application",
    packages=setuptools.find_packages(),
    install_requires=open("requirements.txt").read().splitlines(),
    include_package_data=True,
    scripts=[
        "bin/backend.py",
    ],
    test_suite="test",
)

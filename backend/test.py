import json

from application.app import app
from application.services.database import loan_application

app.config.update({"TESTING": True})
client = app.test_client()


def test_create_application_success():
    response = client.post("/application", json={
        "businessName": "test",
        "yearEstablished": 2019,
        "accountingSoftware": "Xero",
        "token": "123456",
        "loanAmount": 100000
    })
    assert response.status_code == 200
    data = response.get_json()
    assert "application_id" in data
    la = loan_application.get(data["application_id"])
    assert la.pre_assessment == 60 or la.pre_assessment == 20
    assert len(data["balance_sheet"]) > 12


def test_create_application_pre_assessment_100():
    while True:
        response = client.post("/application", json={
            "businessName": "test",
            "yearEstablished": 2019,
            "accountingSoftware": "Xero",
            "token": "123456",
            "loanAmount": 10
        })
        assert response.status_code == 200
        data = response.get_json()
        bs = data["balance_sheet"]
        if bs[0]["asset_value"] > 10:
            break

    la = loan_application.get(data["application_id"])
    assert la.pre_assessment == 100
    assert len(data["balance_sheet"]) > 12


def test_create_application_wrong_accounting_software():
    response = client.post("/application", json={
        "businessName": "test",
        "yearEstablished": 2019,
        "accountingSoftware": "xero",
        "token": "123456",
        "loanAmount": 100000
    })
    assert response.status_code == 500


def test_create_application_wrong_year_established():
    response = client.post("/application", json={
        "businessName": "test",
        "yearEstablished": 2030,
        "accountingSoftware": "xero",
        "token": "123456",
        "loanAmount": 100000
    })
    assert response.status_code == 500


def test_submit_application_success():
    response = client.post("/application", json={
        "businessName": "test",
        "yearEstablished": 2019,
        "accountingSoftware": "Xero",
        "token": "123456",
        "loanAmount": 100000
    })
    response = client.post(f"/application/submit/{response.json['application_id']}")
    assert response.status_code == 200
    data = response.get_json()
    assert data["result"]


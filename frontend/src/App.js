import './App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import StartApplication from './pages/StartApplication';
import PreviewBalanceSheet from './pages/PreviewBalanceSheet';

function App() {

  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/preview" element={<PreviewBalanceSheet />} />
          <Route path="/" element={<StartApplication />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;

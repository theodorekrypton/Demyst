import axios from "axios"

// const baseURL = "http://localhost:12345"
const baseURL = window.location.origin;

const createApplication = async ({ businessName, accountingSoftware, loanAmount, yearEstablished, token }) => {
  return await axios.post(`${baseURL}/application`, { businessName, accountingSoftware, loanAmount, yearEstablished, token });
}

const submitApplication = async ({ applicationID }) => {
  return await axios.post(`${baseURL}/application/submit/${applicationID}`);
}

const api = {
  createApplication,
  submitApplication
}

export default api;

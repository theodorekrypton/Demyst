import React from 'react';
import { useLocation } from 'react-router-dom';

import api from '../../api';

import './styles.css';

const PreviewBalanceSheet = () => {
  const { state } = useLocation();
  const [balanceSheet, setBalanceSheet] = React.useState([]);
  const [applicationID, setApplicationID] = React.useState(null);

  React.useEffect(() => {
    (async () => {
      try {
        const rsp = await api.createApplication(state);
        setBalanceSheet(rsp.data.balance_sheet);
        setApplicationID(rsp.data.application_id);
      } catch (err) {
        console.error(err);
      }
    })();
  }, [state]);

  const getDecision = React.useCallback(() => {
    (async () => {
      try {
        const rsp = await api.submitApplication({ applicationID });
        if (rsp.data.result) {
          alert('The loan application has been succeessfully submitted and approved');
        } else {
          alert('The loan application has been successfully submitted but rejected');
        }
      } catch (err) {
        console.error(err);
      }
    })();
  }, [applicationID]);

  return <div>
    <table>
      <thead>
        <tr>
          <th>Month</th>
          <th>Profit or Loss</th>
          <th>Asset Value</th>
        </tr>
      </thead>
      <tbody>
        {balanceSheet.map(({ year, month, profit_or_loss, asset_value }, i) => (
          <tr key={i}>
            <td>{`${month}/${year}`}</td>
            <td>{profit_or_loss}</td>
            <td>{asset_value}</td>
          </tr>
        ))}
      </tbody>
    </table>
    <input type="button" value="Next" onClick={getDecision} />
  </div>
}

export default PreviewBalanceSheet;
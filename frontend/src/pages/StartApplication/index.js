import React from 'react';
import { useNavigate } from 'react-router-dom';

import './styles.css';


const StartApplication = () => {
  const navigate = useNavigate();

  const [businessName, setBusinessName] = React.useState("demo");

  const businessNameOnChange = React.useCallback((ev) => {
    setBusinessName(ev.target.value.replace(/[^\w\s]/g, ''));
  }, []);

  const [loanAmount, setLoanAmount] = React.useState(10);

  const loanAmountOnChange = React.useCallback((ev) => {
    setLoanAmount(ev.target.value);
  }, []);

  const [accountingSoftware, setAccountingSoftware] = React.useState('Xero');

  const accountingSoftwareOnChange = React.useCallback((ev) => {
    setAccountingSoftware(ev.target.value);
  }, []);

  const [yearEstablished, setYearEstablished] = React.useState(2020);

  const yearEstablishedOnChange = React.useCallback((ev) => {
    setYearEstablished(ev.target.value);
  }, []);

  const [token, setToken] = React.useState("ee977806d7286510da8b9a7492ba58e2484c0ecc");

  const tokenOnChange = React.useCallback((ev) => {
    setToken(ev.target.value)
  }, []);

  return <div >
    <table>
      <tbody>
        <tr>
          <td>Business Name</td>
          <td><input type="text" value={businessName} onChange={businessNameOnChange} /></td>
        </tr>
        <tr>
          <td>Loan Amount</td>
          <td><input type="number" value={loanAmount} onChange={loanAmountOnChange} /></td>
        </tr>
        <tr>
          <td>Accounting Software Provider</td>
          <td>
            <select value={accountingSoftware} onChange={accountingSoftwareOnChange}>
              <option value="Xero">Xero</option>
              <option value="MYOB">MYOB</option>
              <option value="others" disabled>...</option>
            </select>
          </td>
        </tr>
        <tr>
          <td>Year Established</td>
          <td><input type="number" value={yearEstablished} onChange={yearEstablishedOnChange} /></td>
        </tr>
        <tr>
          <td>Token</td>
          <td>
            <input type="password" value={token} onChange={tokenOnChange} />
          </td>
        </tr>
      </tbody>
    </table>
    <input type="button" value="Next" onClick={() => navigate('/preview', { state: { businessName, accountingSoftware, loanAmount, yearEstablished, token } })} />
  </div >
}

export default StartApplication;
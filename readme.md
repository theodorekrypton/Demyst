# Assignment
## How to run
1. Pull the docker image
```
$ docker pull wheatcarrier/demyst && docker tag wheatcarrier/demyst app
```
or run the commands below to build the docker manually
```bash
$ docker build . -t app
```
2. Run the docker
```bash
$ docker run -it -p 12345:12345 app
```
if you want to run at another port,
```bash
$ docker run -it -p $port:$port app [--host=0.0.0.0] --port=$port
```
3. Open `http://localhost:12345/`
4. Click Next to preview the balance sheet
5. Click Next to submit the loan application

## Unit Test
```bash
$ cd backend && pytest test.py
```
## Technical Design
1. The frontend is implemented using `React`, `react-router-dom` and `axios`
2. The backend is implemented using Python and `flask`, exposing two APIs `POST /application` and `POST /application/submit`
    1. `POST /application` is used to create a loan application. The user should provide the backend with the financial information and the loan amount. The request retrieves the balance sheet from the accouting software backend, creates a record in the mocked database and in the same time, calculates the `pre_assessment` and the `profit_summary` to help making decision. The API returns a balance sheet for the user to preview as well as a `application_id`, which is used to confirm and submit the application later.
    2. `POST /application/submit` submits a loan application. The backend requests the decision engine to process the request and returns the final decision. All the data required by the engine are cached in the database. The backend doesn't take any data from the frontend except the `application_id` because the frontend data can be modified and therefore they are not trustworthy. 
3. The database is mocked using a simple Python `dict`, and because the `dict` is not thread safe, the backend uses a lock to guarantee the safety.
4. The decision engine only returns `True` no matter what the inputs are.